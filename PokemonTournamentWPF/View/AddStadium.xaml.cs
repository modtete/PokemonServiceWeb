﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PokemonBusinessLayer;
using PokemonTournamentEntities;
using PokemonTournamentWPF.View;
using PokemonTournamentWPF.UsersControls;
using PokemonTournamentWPF.ViewModel.Views;
using System.Text.RegularExpressions;

namespace PokemonTournamentWPF.View
{
    /// <summary>
    /// Interaction logic for AddStadium.xaml
    /// </summary>
    public partial class AddStadium : Window
    {
        AddStadiumViewModel viewModel;

        public AddStadium(MainViewModel mainWindow)
        {
            InitializeComponent();

            viewModel = new AddStadiumViewModel(mainWindow);
            this.DataContext = viewModel;
            btn_action.Command = viewModel.AddStadiumCommand;
            btn_action.Content = "Add stadium";
        }

        public AddStadium(MainViewModel mainWindow, Stadium stadium)
        {
            InitializeComponent();

            viewModel = new AddStadiumViewModel(mainWindow, stadium);
            this.DataContext = viewModel;
            btn_action.Command = viewModel.UpdateStadiumCommand;
            btn_action.Content = "Update stadium";
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
