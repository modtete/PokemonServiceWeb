﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using PokemonBusinessLayer;
using PokemonTournamentEntities;
using PokemonTournamentWPF.ViewModel.UsersControls;

namespace PokemonTournamentWPF.ViewModel.Views
{
    class AddStadiumViewModel : ViewModelBase
    {
        private MainViewModel mainWindow;
        private Stadium stadium;

        #region "Propriétés accessibles, mappables par la View"

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                base.OnPropertyChanged("Name");
            }
        }

        private int nbPlaces;
        public int NbPlaces
        {
            get { return nbPlaces; }
            set
            {
                nbPlaces = value;
                base.OnPropertyChanged("NbPlaces");
            }
        }

        private EType selectedType;
        public EType SelectedType
        {
            get { return selectedType; }
            set
            {
                selectedType = value;
                base.OnPropertyChanged("SelectedType");
            }
        }

        private ObservableCollection<EType> types;
        public ObservableCollection<EType> Types
        {
            get { return types; }
            set
            {
                types = value;
                base.OnPropertyChanged("Types");
            }
        }
        #endregion

        //Constructor used to add a stadium
        public AddStadiumViewModel(MainViewModel window)
        {
            Name = "";
            NbPlaces = 0;
            SelectedType = EType.Water;
            Types = new ObservableCollection<EType>();
            mainWindow = window;

            foreach(EType t in Enum.GetValues(typeof(EType)).Cast<EType>())
            {
                Types.Add(t);
            }
        }

        //Constructor used to update an existing stadium
        public AddStadiumViewModel(MainViewModel window, Stadium stadium)
        {
            this.stadium = stadium;
            Name = stadium.Name;
            NbPlaces = stadium.NbPlaces;
            SelectedType = stadium.Type;
            Types = new ObservableCollection<EType>();
            mainWindow = window;

            foreach (EType t in Enum.GetValues(typeof(EType)).Cast<EType>())
            {
                Types.Add(t);
            }
        }

        #region "Command add a stadium in the collection"
        private RelayCommand _addStadiumCommand;
        public System.Windows.Input.ICommand AddStadiumCommand
        {
            get
            {
                if (_addStadiumCommand == null)
                {
                    _addStadiumCommand = new RelayCommand(
                        () => this.AddStadium(),
                        () => this.CanAddStadium()
                        );
                }
                return _addStadiumCommand;
            }
        }

        private bool CanAddStadium()
        {
            return true;
        }

        private void AddStadium()
        {
            if (name == "")
            {
                MessageBox.Show("You need to specify the stadium name");
            }
            else
            {               
                try
                {
                    if (mainWindow.Stadiums.Select(stadium => (StadiumViewModel)stadium.DataContext).Where(stadium => stadium.Name.Equals(name)).ToList().Count != 0)
                    {
                        throw new Exception("This stadium already exist");
                    }

                    //Add the stadium in the database
                    BusinessManager manager = new BusinessManager();
                    stadium = new Stadium(name, nbPlaces, selectedType);
                    manager.AddStadium(stadium);

                    //Add the stadium in the mainView stadiums list
                    mainWindow.AddStadiumCollection(stadium);

                    MessageBox.Show("Stadium added","Success",MessageBoxButton.OK,MessageBoxImage.None);
                    Application.Current.MainWindow.Close();
                }
                catch(Exception e)
                {
                    MessageBox.Show(e.Message,"Error",MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        #endregion

        #region "Command update a stadium in the collection"
        private RelayCommand _updateStadiumCommand;
        public System.Windows.Input.ICommand UpdateStadiumCommand
        {
            get
            {
                if (_updateStadiumCommand == null)
                {
                    _updateStadiumCommand = new RelayCommand(
                        () => this.UpdateStadium(),
                        () => this.CanUpdateStadium()
                        );
                }
                return _updateStadiumCommand;
            }
        }

        private bool CanUpdateStadium()
        {
            return true;
        }

        private void UpdateStadium()
        {
            if (name == "")
            {
                MessageBox.Show("You need to specify the stadium name");
            }
            else
            {
                try
                {
                    if (mainWindow.Stadiums.Select(stadium => (StadiumViewModel)stadium.DataContext).Where(stadium => stadium.Name.Equals(name)).ToList().Count != 0)
                    {
                        throw new Exception("This stadium already exist");
                    }

                    //Update a stadium in the database
                    BusinessManager manager = new BusinessManager();
                    stadium.Name = name;
                    stadium.NbPlaces = nbPlaces;
                    stadium.Type = selectedType;
                    
                    manager.UpdateStadium(stadium);

                    //Update a stadium in the mainWindow stadiums list
                    StadiumViewModel s = mainWindow.Stadiums.Where(x => x.StadiumEntity.Id == stadium.Id).Select(x => (StadiumViewModel)x.DataContext).FirstOrDefault();
                    if (s != null)
                    {
                        s.Name = name;
                        s.NbPlaces = nbPlaces;
                        s.Type = selectedType;
                    }

                    MessageBox.Show("Stadium updated", "Success", MessageBoxButton.OK, MessageBoxImage.None);
                    Application.Current.MainWindow.Close();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        #endregion
    }
}
