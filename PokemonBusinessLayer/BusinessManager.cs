﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokemonTournamentEntities;
using PokemonDataAccessLayer;

namespace PokemonBusinessLayer
{
    public class BusinessManager
    {
        DalManager manager;

        public BusinessManager()
        {
            manager = DalManager.getInstance(DataProvider.STUBBED);
            //manager = DalManager.getInstance(DataProvider.SQLSERVER);
        }

        public void AddStadium(Stadium stadium)
        {
            manager.AddStadium(stadium);
        }
        public void UpdateStadium(Stadium stadium)
        {
            manager.UpdateStadium(stadium);
        }
        public void RemoveStadium(Stadium stadium)
        {
            manager.RemoveStadium(stadium);
        }

        public void AddPokemon(Pokemon pokemon)
        {
            manager.AddPokemon(pokemon);
        }
        public void UpdatePokemon(Pokemon pokemon)
        {
            manager.UpdatePokemon(pokemon);
        }
        public void RemovePokemon(Pokemon pokemon)
        {
            manager.RemovePokemon(pokemon);
        }

        //Ces fonctions renvoient des objets
        public List<Pokemon> GetAllPokemons()
        {
            return manager.GetAllPokemons();
        }

        public List<Pokemon> GetAllPokemonsByType(EType type)
        {
            return manager.GetAllPokemons().Where(p => p.Types.Contains(type)).ToList();
        }

        public List<Pokemon> GetAllPokemonsByTypes(List<EType> types)
        {
            List<Pokemon> pokemonsName = new List<Pokemon>();
            Pokemon[] pokemons = manager.GetAllPokemons().ToArray();
            int i = 0,
                j = 0,
                nb_pokemons = pokemons.Length,
                nb_types = types.Count;

            if (nb_types != 0)
            {
                for (i = 0; i < nb_pokemons; ++i)
                {
                    for (j = 0; j < nb_types; ++j)
                    {
                        if (pokemons[i].Types.Contains(types[j]))
                        {
                            pokemonsName.Add(pokemons[i]);
                        }
                    }
                }
            }
            pokemonsName = pokemonsName.Distinct().ToList();
            return pokemonsName;
        }

        public List<Pokemon> GetAllStrongPokemons()
        {
            //return manager.GetAllPokemons().Where(p => p.Characteristics.First(f => f.Attribute == EAttribute.Strengh).Value > 5).ToList();
            return manager.GetAllPokemons().Where(p => p.Strengh > 5).ToList();
        }

        public List<Stadium> GetAllStadiums()
        {
            return manager.GetAllStades();
        }

        public List<Match> GetAllMatchs()
        {
            return manager.GetAllMatchs();
        }

        // Ces fonctions renvoient des string
        public List<string> GetAllPokemonsName()
        {
            return manager.GetAllPokemons().Select(s => s.Name).ToList();
        }

        public List<string> GetAllPokemonsByTypeName(EType type)
        {
            return manager.GetAllPokemons().Where(p => p.Types.Contains(type)).Select(s => s.Name).ToList();
        }

        public List<string> GetAllPokemonsByTypesName(List<EType> types)
        {
            List<string> pokemonsName = new List<string>();
            Pokemon[] pokemons = manager.GetAllPokemons().ToArray();
            int i = 0,
                j = 0,
                nb_pokemons = pokemons.Length,
                nb_types = types.Count;          
            
            if(nb_types != 0)
            {
                for(i = 0; i < nb_pokemons; ++i)
                {
                    for (j = 0; j < nb_types; ++j)
                    {
                        if (pokemons[i].Types.Contains(types[j]))
                        {
                            pokemonsName.Add(pokemons[i].Name);
                        }
                    }
                }
            }

            return pokemonsName;
        }

        public List<string> GetAllStrongPokemonsName()
        {
            //return manager.GetAllPokemons().Where(p => p.Characteristics.First(f => f.Attribute == EAttribute.Strengh).Value > 5).Select(s => s.Name).ToList();
            return manager.GetAllPokemons().Where(p => p.Strengh > 5).Select(p=>p.ToString()).ToList();
        }

        public List<string> GetAllStadiumsName()
        {
            return manager.GetAllStades().Select(s => s.Name).ToList();
        }

        public List<string> GetAllMatchsBigStadium()
        {
            return manager.GetAllMatchs().Where(m => m.Stadium.NbPlaces >= 200).Select(s => s.ToString()).ToList();
        }

        public Boolean Connexion(String login, String password)
        {
            User user = manager.GetUserByLogin(login);
            if (user != null && user.Password == password)
                return true;
            return false;
        }
    }
}