﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PokemonTournamentEntities;
using PokemonTournamentWPF.ViewModel.UsersControls;

namespace PokemonTournamentWPF.UsersControls
{
    /// <summary>
    /// Interaction logic for MatchView.xaml
    /// </summary>
    public partial class MatchView : UserControl
    {
        public Match MatchEntity { get; private set; }

        public MatchView(Match match)
        {
            InitializeComponent();

            MatchEntity = match;
            this.DataContext = new MatchViewModel(match);
        }
    }
}
