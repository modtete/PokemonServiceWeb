﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonTournamentEntities
{
    public class Trainer : EntityObject
    {
        public string Name { get; set; }
        public int Score { get; set; }

        public Trainer() : base()
        {

        }

        public Trainer(string name) : base()
        {
            Name = name;
        }

    }
}
