﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonTournamentEntities
{
    public class Match : EntityObject
    {
        public int IdWinner { get; set; }
        public ETournamentPhase TournamentPhase { get; set; }
        public Pokemon Pokemon1 { get; set; }
        public Pokemon Pokemon2 { get; set; }
        public Stadium Stadium { get; set; }
        private static List<List<double>> weakness;
        private static Random rand;


        //A revoir
        public Match(ETournamentPhase tournamentPhase, Pokemon p1, Pokemon p2, Stadium stadium) : base()
        {
            IdWinner = -1;
            TournamentPhase = tournamentPhase;
            Pokemon1 = p1;
            Pokemon2 = p2;
            Stadium = stadium;
            PlayMatch();
        }


        public static void init()
        {
            weakness = new List<List<double>>();
            for (int i = 0; i < 18; ++i)
            {
                weakness.Add(new List<double>());

                for (int j = 0; j < 18; ++j)
                {
                    weakness[i].Add(1);
                }
            }



            rand = new Random();
        }

        private void PlayMatch()
        {
            double pok1Chances = rand.Next();
            double pok2Chances = rand.Next();
            double mult1 = 1;
            double mult2 = 1;
            foreach (EType typeAtt in Pokemon1.Types)
            {
                foreach (EType typeDef in Pokemon2.Types)
                {
                    mult1 *= weakness[(int)typeAtt][(int)typeDef];
                }
                if (typeAtt.Equals(Stadium.Type))
                    mult1 *= 1.5;
            }

            foreach (EType typeAtt in Pokemon2.Types)
            {
                foreach (EType typeDef in Pokemon1.Types)
                {
                    mult2 *= weakness[(int)typeAtt][(int)typeDef];
                }
                if (typeAtt.Equals(Stadium.Type))
                    mult2 *= 1.5;
            }

            pok1Chances *= mult1;
            pok2Chances *= mult2;
            IdWinner = pok1Chances < pok2Chances ? Pokemon2.Id : Pokemon1.Id;


        }

        public Pokemon GetWinner()
        {
            return Pokemon1.Id == IdWinner ? Pokemon1 : Pokemon2;
        }

        public override string ToString()
        {
            if (IdWinner == -1)
            {
                return "The match has not yet been played !";
            }
            else
            {
                String winner = Pokemon1.Id == IdWinner ? Pokemon1.Name : Pokemon2.Name;
                return TournamentPhase + " : " + Pokemon1.Name + " vs " + Pokemon2.Name + "  Win: " + winner;
            }
        }
    }
}
