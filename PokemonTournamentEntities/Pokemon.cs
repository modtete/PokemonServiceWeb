﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PokemonTournamentEntities
{
    [Serializable]
    public class Pokemon : EntityObject
    {
        public string Name { get; set; }        
        public List<EType> Types { get; set; }
        //public List<Characteristic> Characteristics { get; set; }
        public int Life { get; set; }
        public int Strengh { get; set; }
        public int Defense { get; set; }
        public int Speed { get; set; }

        public Pokemon() : base()
        {

        }

        /*public Pokemon(string name, List<Characteristic> characteristics, List<EType> types)
            : base()
        {
            Name = name;
            Types = types;
            Characteristics = characteristics;            
        }*/
        public Pokemon(string name, int life, int strengh, int defense, int speed, List<EType> types)
            : base()
        {
            Name = name;
            Types = types;
            Life = life;
            Strengh = strengh;
            Defense = defense;
            Speed = speed;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
