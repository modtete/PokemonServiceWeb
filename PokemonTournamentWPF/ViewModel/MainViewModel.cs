﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokemonBusinessLayer;
using PokemonTournamentEntities;
using System.Collections.ObjectModel;
using PokemonTournamentWPF.UsersControls;
using System.Windows.Controls;
using System.Windows;
using PokemonTournamentWPF.View;

namespace PokemonTournamentWPF.ViewModel
{
    //This is class is used to display objects in the main window
    public class MainViewModel : ViewModelBase
    {
        private BusinessManager manager;

        #region "Propriétés accessibles, mappables par la View"

        private ObservableCollection<PokemonView> pokemons;
        public ObservableCollection<PokemonView> Pokemons
        {
            get { return pokemons; }
            set
            {
                pokemons = value;
                base.OnPropertyChanged("Pokemons");
            }
        }

        private ObservableCollection<StadiumView> stadiums;
        public ObservableCollection<StadiumView> Stadiums
        {
            get { return stadiums; }
            set
            {
                stadiums = value;
                base.OnPropertyChanged("Stadiums");
            }
        }

        private ObservableCollection<MatchView> matchs;
        public ObservableCollection<MatchView> Matchs
        {
            get { return matchs; }
            set
            {
                matchs = value;
                base.OnPropertyChanged("Matchs");
            }
        }

        private ObservableCollection<CheckBox> pokemonsFiltering;
        public ObservableCollection<CheckBox> PokemonsFiltering
        {
            get { return pokemonsFiltering; }
            set
            {
                pokemonsFiltering = value;
                base.OnPropertyChanged("PokemonsFiltering");
            }
        }
        #endregion

        public MainViewModel()
        {
            manager = new BusinessManager();
            pokemons = new ObservableCollection<PokemonView>();
            stadiums = new ObservableCollection<StadiumView>();
            matchs = new ObservableCollection<MatchView>();
            pokemonsFiltering = new ObservableCollection<CheckBox>();

            InitPokemonsView();
            InitPokemonsFilteringView();
            InitStadiumsView();
            InitMatchsView();
        }

        //Initialize all view with pokemons/stadiums/matchs..
        #region "Init views"
        public void InitPokemonsView(List<EType> filters = null)
        {
            List<Pokemon> pokemonsToDisplay;

            if(filters == null){
                pokemonsToDisplay = manager.GetAllPokemons();
            } else {
                pokemonsToDisplay = manager.GetAllPokemonsByTypes(filters);
            }

            pokemons.Clear();
            foreach (Pokemon p in pokemonsToDisplay)
            {
                pokemons.Add(new PokemonView(p));
            }            
        }

        public void InitPokemonsFilteringView()
        {
            pokemonsFiltering.Clear();
            foreach (EType type in Enum.GetValues(typeof(EType)))
            {
                CheckBox c = new CheckBox();
                c.Checked += cb_filtering_pokemons;
                c.Unchecked += cb_filtering_pokemons;
                c.Content = type;
                pokemonsFiltering.Add(c);
            }
        }

        private void cb_filtering_pokemons(object sender, RoutedEventArgs e)
        {
            List<EType> filters = pokemonsFiltering.Where(cb => (bool)cb.IsChecked).Select(cb => (EType)cb.Content).ToList();
            if (filters.Count != 0)
            {
                InitPokemonsView(filters);
            }
            else
            {
                InitPokemonsView();
            }

        }

        public void InitStadiumsView()
        {
            stadiums.Clear();
            foreach (Stadium s in manager.GetAllStadiums())
            {
                stadiums.Add(new StadiumView(s));
            }
        }

        public void InitMatchsView()
        {
            matchs.Clear();
            foreach (Match m in manager.GetAllMatchs())
            {
                matchs.Add(new MatchView(m));
            }
        }
        #endregion

        // Command add a stadium in the collection
        #region "Add stadium"
        private RelayCommand _addStadiumCommand;
        public System.Windows.Input.ICommand AddStadiumCommand
        {
            get
            {
                if (_addStadiumCommand == null)
                {
                    _addStadiumCommand = new RelayCommand(
                        () => this.AddStadium(),
                        () => this.CanAddStadium()
                        );
                }
                return _addStadiumCommand;
            }
        }

        private bool CanAddStadium()
        {
            return true;
        }

        private void AddStadium()
        {
            AddStadium window = new AddStadium(this);
            window.Show();
        }
        #endregion

        // Command to save pokemons
        #region "Save pokemons"
        private RelayCommand _savePokemonsCommand;
        public System.Windows.Input.ICommand SavePokemonsCommand
        {
            get
            {
                if (_addStadiumCommand == null)
                {
                    _addStadiumCommand = new RelayCommand(
                        () => this.SavePokemons(),
                        () => this.CanSavePokemons()
                        );
                }
                return _addStadiumCommand;
            }
        }

        private bool CanSavePokemons()
        {
            return true;
        }

        private void SavePokemons()
        {
            XMLSave fenetre = new XMLSave();
            fenetre.Show();
        }
        #endregion

    }
}
