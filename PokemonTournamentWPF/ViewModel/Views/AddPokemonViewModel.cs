﻿using PokemonBusinessLayer;
using PokemonTournamentEntities;
using PokemonTournamentWPF.ViewModel.UsersControls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PokemonTournamentWPF.ViewModel.Views
{
    class AddPokemonViewModel : ViewModelBase
    {
        private MainViewModel mainWindow;
        private Pokemon pokemon;

        #region "Propriétés accessibles, mappables par la View"

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                base.OnPropertyChanged("Name");
            }
        }

        private ObservableCollection<EType> typesAvailables;
        public ObservableCollection<EType> TypesAvailables
        {
            get { return typesAvailables; }
            set
            {
                typesAvailables = value;
                base.OnPropertyChanged("TypesAvailables");
            }
        }

        private EType selectedTypesAvailables;
        public EType SelectedTypesAvailables
        {
            get { return selectedTypesAvailables; }
            set
            {
                selectedTypesAvailables = value;
                base.OnPropertyChanged("SelectedTypesAvailables");
            }
        }

        private ObservableCollection<EType> typesSelected;
        public ObservableCollection<EType> TypesSelected
        {
            get { return typesSelected; }
            set
            {
                typesSelected = value;
                base.OnPropertyChanged("TypesSelected");
            }
        }

        private EType selectedTypesSelected;
        public EType SelectedTypesSelected
        {
            get { return selectedTypesSelected; }
            set
            {
                selectedTypesSelected = value;
                base.OnPropertyChanged("SelectedTypesSelected");
            }
        }
                
        private int life;
        public int Life
        {
            get { return life; }
            set
            {
                life = value;
                base.OnPropertyChanged("Life");
            }
        }
        private int strengh;
        public int Strengh
        {
            get { return strengh; }
            set
            {
                strengh = value;
                base.OnPropertyChanged("Strengh");
            }
        }
        private int defense;
        public int Defense
        {
            get { return defense; }
            set
            {
                defense = value;
                base.OnPropertyChanged("Defense");
            }
        }
        private int speed;
        public int Speed
        {
            get { return speed; }
            set
            {
                speed = value;
                base.OnPropertyChanged("Speed");
            }
        }
        #endregion

        //Constructor used to add a pokemon
        public AddPokemonViewModel(MainViewModel window)
        {
            Name = "";
            Life = 0;
            Strengh = 0;
            Defense = 0;
            Speed = 0;

            TypesAvailables = new ObservableCollection<EType>();
            TypesSelected = new ObservableCollection<EType>();

            mainWindow = window;

            foreach (EType t in Enum.GetValues(typeof(EType)).Cast<EType>())
            {
                TypesAvailables.Add(t);
            }
        }

        //Constructor used to update an existing pokemon
        public AddPokemonViewModel(MainViewModel window, Pokemon pokemon)
        {
            this.pokemon = pokemon;
            Name = pokemon.Name;
            Life = pokemon.Life;
            Strengh = pokemon.Strengh;
            Defense = pokemon.Defense;
            Speed = pokemon.Speed;

            TypesAvailables = new ObservableCollection<EType>();
            TypesSelected = new ObservableCollection<EType>();

            mainWindow = window;

            foreach (EType t in Enum.GetValues(typeof(EType)).Cast<EType>())
            {
                if(pokemon.Types.Contains(t))
                {
                    TypesSelected.Add(t);
                }
                else
                {
                    TypesAvailables.Add(t);
                }
            }
        }

        #region "Command add a pokemon in the collection"
        private RelayCommand _addPokemonCommand;
        public System.Windows.Input.ICommand AddPokemonCommand
        {
            get
            {
                if (_addPokemonCommand == null)
                {
                    _addPokemonCommand = new RelayCommand(
                        () => this.AddPokemon(),
                        () => this.CanAddPokemon()
                        );
                }
                return _addPokemonCommand;
            }
        }

        private bool CanAddPokemon()
        {
            return true;
        }

        private void AddPokemon()
        {
            if (name == "")
            {
                MessageBox.Show("You need to specify the pokemon name");
            }
            else
            {
                try
                {
                    if (mainWindow.Pokemons.Select(stadium => (PokemonViewModel)stadium.DataContext).Where(pokemon => pokemon.Name.Equals(name)).ToList().Count != 0)
                    {
                        throw new Exception("This pokemon already exist");
                    }
                    //Add the pokemon in the database
                    BusinessManager manager = new BusinessManager();
                    pokemon = new Pokemon(name, life, strengh, defense, speed, typesSelected.ToList());
                    manager.AddPokemon(pokemon);

                    //Add the pokemon in the mainView pokemons list
                    mainWindow.AddPokemonCollection(pokemon);

                    MessageBox.Show("Pokemon added","Success", MessageBoxButton.OK, MessageBoxImage.None);
                    Application.Current.MainWindow.Close();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message,"Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        #endregion

        #region "Command update a pokemon in the collection"
        private RelayCommand _updatePokemonCommand;
        public System.Windows.Input.ICommand UpdatePokemonCommand
        {
            get
            {
                if (_updatePokemonCommand == null)
                {
                    _updatePokemonCommand = new RelayCommand(
                        () => this.UpdatePokemon(),
                        () => this.CanUpdatePokemon()
                        );
                }
                return _updatePokemonCommand;
            }
        }

        private bool CanUpdatePokemon()
        {
            return true;
        }

        private void UpdatePokemon()
        {
            if (name == "")
            {
                MessageBox.Show("You need to specify the pokemon name");
            }
            else
            {
                try
                {
                    if (mainWindow.Pokemons.Select(stadium => (PokemonViewModel)stadium.DataContext).Where(pokemon => pokemon.Name.Equals(name)).ToList().Count != 0)
                    {
                        throw new Exception("This pokemon already exist");
                    }

                    //Update pokemon in the database
                    BusinessManager manager = new BusinessManager();
                    pokemon.Name = name;
                    pokemon.Life = life;
                    pokemon.Strengh = strengh;
                    pokemon.Defense = defense;
                    pokemon.Speed = speed;
                    pokemon.Types = typesSelected.ToList();

                    manager.UpdatePokemon(pokemon);

                    //Update the pokemonViewModel in the MainWindow pokemons list
                    PokemonViewModel p = mainWindow.Pokemons.Where(x => x.PokemonEntity.Id == pokemon.Id).Select(x => (PokemonViewModel)x.DataContext).FirstOrDefault();
                    if(p != null)
                    {
                        p.Name = name;
                        p.Life = life;
                        p.Strengh = strengh;
                        p.Defense = defense;
                        p.Speed = speed;
                        p.Types = typesSelected;
                    }

                    MessageBox.Show("Pokemon updated","Success", MessageBoxButton.OK, MessageBoxImage.None);
                    Application.Current.MainWindow.Close();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message,"Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        #endregion

        #region "Command : Add a type in the selectedTypes collection"
        private RelayCommand _addTypeCommand;
        public System.Windows.Input.ICommand AddTypeCommand
        {
            get
            {
                if (_addTypeCommand == null)
                {
                    _addTypeCommand = new RelayCommand(
                        () => this.AddType(),
                        () => this.CanAddType()
                        );
                }
                return _addTypeCommand;
            }
        }

        private bool CanAddType()
        {
            return true;
        }

        private void AddType()
        {
            if(!typesSelected.Contains(selectedTypesAvailables))
            {
                typesSelected.Add(selectedTypesAvailables);
                typesAvailables.Remove(selectedTypesAvailables);
            }
        }
        #endregion

        #region "Command : Remove a type in the selectedTypes collection"
        private RelayCommand _removeypeCommand;
        public System.Windows.Input.ICommand RemoveTypeCommand
        {
            get
            {
                if (_removeypeCommand == null)
                {
                    _removeypeCommand = new RelayCommand(
                        () => this.RemoveType(),
                        () => this.CanRemoveType()
                        );
                }
                return _removeypeCommand;
            }
        }

        private bool CanRemoveType()
        {
            return true;
        }

        private void RemoveType()
        {
            if(!typesAvailables.Contains(selectedTypesSelected))
            {
                typesAvailables.Add(selectedTypesSelected);
                typesSelected.Remove(selectedTypesSelected);
            }
        }
        #endregion
    }
}
