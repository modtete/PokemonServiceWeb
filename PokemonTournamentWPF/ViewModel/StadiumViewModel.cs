﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokemonTournamentEntities;

namespace PokemonTournamentWPF.ViewModel
{
    //This is class is used to display stadiums characteristics in the main window
    public class StadiumViewModel : ViewModelBase
    {
        #region "Propriétés accessibles, mappables par la View"

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    name = value;
                    base.OnPropertyChanged("Name");
                }
            }
        }

        private int nbPlaces;
        public int NbPlaces
        {
            get { return nbPlaces; }
            set
            {
                nbPlaces = value;
                base.OnPropertyChanged("NbPlaces");
            }
        }

        private EType type;
        public EType Type
        {
            get { return type; }
            set
            {
                type = value;
                base.OnPropertyChanged("Type");
            }
        }
        #endregion

        public StadiumViewModel(Stadium stadium)
        {
            name = stadium.Name;
            nbPlaces = stadium.NbPlaces;
            type = stadium.Type;
        }
    }
}
