﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PokemonBusinessLayer;
using PokemonTournamentEntities;

namespace PokemonTournamentWPF
{
    //static ListView myList;
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BusinessManager businessManager;

        public MainWindow()
        {
            InitializeComponent();
            businessManager = new BusinessManager();

            InitPokemonsView();
            InitStadiumsView();
            InitMatchsView();

            Pokemons.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
        }

        public void InitPokemonsView()
        {
            list_view_pokemons.ItemsSource = null;
            list_view_pokemons.ItemsSource = businessManager.GetAllPokemons();

            foreach (EType type in Enum.GetValues(typeof(EType)))
            {
                CheckBox c = new CheckBox();
                c.Checked += cb_filtering_pokemons;
                c.Unchecked += cb_filtering_pokemons;
                c.Content = type;
                stack_types_filtering_pokemons.Children.Add(c);
            }
        }

        public void InitStadiumsView()
        {
            list_view_stadiums.ItemsSource = null;
            list_view_stadiums.ItemsSource = businessManager.GetAllStadiums();
        }

        public void InitMatchsView()
        {
            list_view_matchs.ItemsSource = null;
            list_view_matchs.ItemsSource = businessManager.GetAllMatchs();            
        }

        private void Pokemons_Click(object sender, RoutedEventArgs e)
        {
            Grid_view_pokemons.Visibility = Visibility.Visible;
            Grid_view_stadiums.Visibility = Visibility.Collapsed;
            Grid_view_matchs.Visibility = Visibility.Collapsed;
        }

        private void Stadiums_Click(object sender, RoutedEventArgs e)
        {    
            Grid_view_pokemons.Visibility = Visibility.Collapsed;
            Grid_view_stadiums.Visibility = Visibility.Visible;
            Grid_view_matchs.Visibility = Visibility.Collapsed;
        }

        private void Matchs_Click(object sender, RoutedEventArgs e)
        {
            Grid_view_pokemons.Visibility = Visibility.Collapsed;
            Grid_view_stadiums.Visibility = Visibility.Collapsed;
            Grid_view_matchs.Visibility = Visibility.Visible;
        }

        private void Characteristics_Click(object sender, RoutedEventArgs e)
        {
            //listView.ItemsSource = businessManager.;
        }

        private void cb_filtering_pokemons(object sender, RoutedEventArgs e)
        {
            List<CheckBox> filteringCheckboxs = stack_types_filtering_pokemons.Children.Cast<CheckBox>().ToList();
            List<EType> filtering_types = filteringCheckboxs.Where(cb => (bool)cb.IsChecked).Select(cb => (EType)cb.Content).ToList();
            if(filtering_types.Count != 0)
            {
                list_view_pokemons.ItemsSource = businessManager.GetAllPokemonsByTypesName(filtering_types);
            }
            else
            {
                list_view_pokemons.ItemsSource = businessManager.GetAllPokemonsName();
            }
            
        }

        private void MenuItem_save_Click(object sender, RoutedEventArgs e)
        {
            XMLSave fenetre = new XMLSave();
            fenetre.Show();
        }

        private void btn_addStadium_Click(object sender, RoutedEventArgs e)
        {
            AddStadium window = new AddStadium(this);
            window.Show();
        }
    }
}
