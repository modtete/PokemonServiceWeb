﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokemonTournamentEntities;
using System.Collections.ObjectModel;

namespace PokemonTournamentWPF.ViewModel.UsersControls
{
    //This is class is used to display pokemons characteristics in the main window
    public class PokemonViewModel : ViewModelBase
    {
        #region "Propriétés accessibles, mappables par la View"

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    name = value;
                    base.OnPropertyChanged("Name");
                }
            }
        }

        private ObservableCollection<EType> types;
        public ObservableCollection<EType> Types
        {
            get { return types; }
            set
            {
                types = value;
                base.OnPropertyChanged("Types");
            }
        }
        
        private int life;
        public int Life
        {
            get { return life; }
            set
            {
                life = value;
                base.OnPropertyChanged("Life");
            }
        }
        private int strengh;
        public int Strengh
        {
            get { return strengh; }
            set
            {
                strengh = value;
                base.OnPropertyChanged("Strengh");
            }
        }
        private int defense;
        public int Defense
        {
            get { return defense; }
            set
            {
                defense = value;
                base.OnPropertyChanged("Defense");
            }
        }
        private int speed;
        public int Speed
        {
            get { return speed; }
            set
            {
                speed = value;
                base.OnPropertyChanged("Speed");
            }
        }
        #endregion

        public PokemonViewModel(Pokemon pokemon)
        {
            types = new ObservableCollection<EType>();
            //characteristics = new ObservableCollection<Characteristic>();
            life = pokemon.Life;
            strengh = pokemon.Strengh;
            defense = pokemon.Defense;
            speed = pokemon.Speed;
            name = pokemon.Name;

            foreach(EType t in pokemon.Types)
            {
                types.Add(t);
            }
            /*foreach(Characteristic c in pokemon.Characteristics)
            {
                characteristics.Add(c);
            }*/
        }
    }
}
