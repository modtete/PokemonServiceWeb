﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PokemonBusinessLayer;

namespace PokemonTournamentWPF
{
    /// <summary>
    /// Logique d'interaction pour Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        BusinessManager businessManager;

        public Login()
        {
            InitializeComponent();
            businessManager = new BusinessManager();
        }

        private void btnConnexion_Click(object sender, RoutedEventArgs e)
        {
            if(businessManager.Connexion(xlogin.Text.ToLower(),xpassword.Password))
            {
                MainWindow win = new MainWindow();
                win.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Wrong Login or password", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
