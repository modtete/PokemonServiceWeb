﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokemonTournamentEntities;

namespace PokemonTournamentWPF.ViewModel.UsersControls
{
    //This is class is used to display matchs characteristics in the main window
    public class MatchViewModel : ViewModelBase
    {
        #region "Propriétés accessibles, mappables par la View"

        private string matchDescription;
        public string MatchDescription
        {
            get { return matchDescription; }
            set
            {
                if (value != matchDescription)
                {
                    matchDescription = value;
                    base.OnPropertyChanged("MatchDescription");
                }
            }
        }
        
        #endregion

        public MatchViewModel(Match match)
        {
            matchDescription = match.ToString();
        }
    }
}
