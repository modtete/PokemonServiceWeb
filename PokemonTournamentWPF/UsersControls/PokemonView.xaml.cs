﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PokemonTournamentEntities;
using PokemonTournamentWPF.ViewModel.UsersControls;

namespace PokemonTournamentWPF.UsersControls
{
    /// <summary>
    /// Interaction logic for PokemonView.xaml
    /// </summary>
    public partial class PokemonView : UserControl
    {
        public Pokemon PokemonEntity { get; private set; }
        public PokemonView(Pokemon pokemon)
        {
            InitializeComponent();

            PokemonEntity = pokemon;
            this.DataContext = new PokemonViewModel(pokemon);
        }
    }
}
