﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokemonTournamentEntities;

namespace PokemonDataAccessLayer
{
    public enum DataProvider
    {
        ORACLE,
        SQLSERVER,
        STUBBED
    }

    
    public class DalManager
    {
        #region Attribut/Propriété
        private IDal _dal;

        private static DalManager instance = null;
        private static object padlock = new object();

        public IDal DataAccesLayer
        {
            get { return _dal;}
            set { _dal = value; }
        }
        #endregion

        private DalManager(DataProvider chosen)
        {
            if(chosen == DataProvider.ORACLE)
            {
              
            }
            else if (chosen == DataProvider.SQLSERVER)
            {
                //DataAccesLayer = new DalSQLServeur(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=..\PokemonDataBase.mdf;Integrated Security=True;Connect Timeout=30");
                //DataAccesLayer = new DalSQLServeur(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Max\Source\Repos\PokemonDataBase.mdf;Integrated Security=True;Connect Timeout=30");
                DataAccesLayer = new DalSQLServeur(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\lixan_000\Desktop\cours\c#\tp_gitlab_update\PokemonDataBase.mdf;Integrated Security=True;Connect Timeout=30");
            }
            else if(chosen == DataProvider.STUBBED)
            {
                DataAccesLayer = new DalSTubbed();
            }

            CreateWorld();
        }

        public static DalManager getInstance(DataProvider chosen)
        {
            if(instance == null)
            {
                lock(padlock)
                {
                    if(instance == null)
                    {
                        instance = new DalManager(chosen);
                    }
                }        
            }
            return instance;
        }
        

        #region Caractéristique

        public List<Characteristic> GetAllCharacteristics()
        {
            return DataAccesLayer.GetAllCharacteristics();
        }

        #endregion

        #region Pokemon

        public void AddPokemon(Pokemon pokemon)
        {
            DataAccesLayer.AddPokemon(pokemon);
        }
        public void RemovePokemon(Pokemon pokemon)
        {
            DataAccesLayer.RemovePokemon(pokemon);
        }
        public void UpdatePokemon(Pokemon pokemon)
        {
            DataAccesLayer.UpdatePokemon(pokemon);
        }
        public List<Pokemon> GetAllPokemons()
        {
            return DataAccesLayer.GetAllPokemons();
        }

        #endregion

        #region Match

        void AddMatch(Match match)
        {
            DataAccesLayer.AddMatch(match);
        }
        void RemoveMatch(Match match)
        {
            DataAccesLayer.RemoveMatch(match);
        }
        void UpdateMatch(Match match)
        {
            DataAccesLayer.UpdateMatch(match);
        }
        public List<Match> GetAllMatchs()
        {
            return DataAccesLayer.GetAllMatchs();
        }

        #endregion

        #region Stade

        public void AddStadium(Stadium stadium)
        {
            DataAccesLayer.AddStadium(stadium);
        }
        public void RemoveStadium(Stadium stadium)
        {
            DataAccesLayer.RemoveStadium(stadium);
        }
        public void UpdateStadium(Stadium stadium)
        {
            DataAccesLayer.UpdateStadium(stadium);
        }
        public List<Stadium> GetAllStades()
        {
            return DataAccesLayer.GetAllStades();
        }

        #endregion

        #region User
        public void AddUser(User user)
        {
            DataAccesLayer.AddUser(user);
        }
        public void RemoveUser(User user)
        {
            DataAccesLayer.RemoveUser(user);
        }
        public void UpdateUser(User user)
        {
            DataAccesLayer.UpdateUser(user);
        }

        public User GetUserByLogin(String login)
        {
            return DataAccesLayer.GetUserByLogin(login);
        }
        #endregion

        private void CreateWorld()
        {
            /*
            listCharacteristics.Add(new Characteristic(EAttribute.Strengh, 100));
            listCharacteristics.Add(new Characteristic(EAttribute.Defense, 50));
            listCharacteristics.Add(new Characteristic(EAttribute.Speed, 80));
            listCharacteristics.Add(new Characteristic(EAttribute.Life, 200));

            Pokemon p1 = new Pokemon("Electhor", listCharacteristics, new List<EType>() { EType.Electric, EType.Flying });
            Pokemon p2 = new Pokemon("Brasegali", listCharacteristics, new List<EType>() { EType.Fire });
            Pokemon p3 = new Pokemon("Onigali", listCharacteristics, new List<EType>() { EType.Ice });
            Pokemon p4 = new Pokemon("Lucario", listCharacteristics, new List<EType>() { EType.Ground });
            Pokemon p5 = new Pokemon("Tortank", listCharacteristics, new List<EType>() { EType.Water, EType.Ground });
            Pokemon p6 = new Pokemon("Germinion", listCharacteristics, new List<EType>() { EType.Plant });
            Pokemon p7 = new Pokemon("Groudon", listCharacteristics, new List<EType>() { EType.Fire, EType.Ground });
            Pokemon p8 = new Pokemon("Kyogre", listCharacteristics, new List<EType>() { EType.Water, EType.Flying });
            listPokemons.Add(p1);
            listPokemons.Add(p2);
            listPokemons.Add(p3);
            listPokemons.Add(p4);
            listPokemons.Add(p5);
            listPokemons.Add(p6);
            listPokemons.Add(p7);
            listPokemons.Add(p8);

            Stadium s1 = new Stadium("Stadium neutre", 75000, EType.Neutral);
            Stadium s2 = new Stadium("Stadium éclair", 50000, EType.Electric);
            Stadium s3 = new Stadium("Stadium aquatique", 90000, EType.Water);
            Stadium s4 = new Stadium("Stadium volcan", 60000, EType.Fire);

            listStadiums.Add(s1);
            listStadiums.Add(s2);
            listStadiums.Add(s3);
            listStadiums.Add(s4);

            Match m1 = new Match(p1.Id, ETournamentPhase.Quarterfinals, p1, p2,  s2);
            Match m2 = new Match(p3.Id, ETournamentPhase.Quarterfinals, p3, p4, s1);
            Match m3 = new Match(p5.Id, ETournamentPhase.Quarterfinals, p5, p6,  s3);
            Match m4 = new Match(p7.Id, ETournamentPhase.Quarterfinals, p7, p8, s4);
            Match m5 = new Match(p1.Id, ETournamentPhase.Semifinals, p1, p3,s4);
            Match m6 = new Match(p5.Id, ETournamentPhase.Semifinals, p5, p7,  s2);
            Match m7 = new Match(p5.Id, ETournamentPhase.Final, p1, p5,  s1);
            listMatchs.Add(m1);
            listMatchs.Add(m2);
            listMatchs.Add(m3);
            listMatchs.Add(m4);
            listMatchs.Add(m5);
            listMatchs.Add(m6);
            listMatchs.Add(m7);

            User u1 = new User("Maxime", "Chomont", "", "");
            listUsers.Add(u1);*/
            DataAccesLayer.CreateWorld();
        }
        
    }
}