﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PokemonTournamentEntities;
using PokemonTournamentWPF.ViewModel.UsersControls;
using PokemonTournamentWPF.ViewModel.Views;

namespace PokemonTournamentWPF.UsersControls
{
    /// <summary>
    /// Interaction logic for StadiumView.xaml
    /// </summary>
    public partial class StadiumView : UserControl
    {
        public Stadium StadiumEntity { get; private set; }
        public StadiumView(Stadium stadium)
        {
            InitializeComponent();

            StadiumEntity = stadium;
            this.DataContext = new StadiumViewModel(stadium);
        }
    }
}
